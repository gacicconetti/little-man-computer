export class Examples {
  examples;

  constructor() {
    this.examples = [
      {
        name: 'Fibonacci',
        id: 'fibonacci',
        code: [
          'LDA A',
          'OUT',
          'LDA B',
          'OUT',
          'START LDA B',
          'ADD A',
          'STA RES',
          'LDA B',
          'STA A',
          'LDA RES',
          'STA B',
          'OUT',
          'BRA START',
          'RES DAT',
          'A DAT 1',
          'B DAT 1'
        ]
      },
      {
        name: 'Sum',
        id: 'sum',
        code: [
          'INP',
          'STA A',
          'INP',
          'ADD A',
          'OUT',
          'HLT',
          'A DAT',
          'B DAT',
        ]
      }, {
        name: 'Sub',
        id: 'sub',
        code: [
          'INP',
          'STA A',
          'INP // this - A',
          'SUB A',
          'STA RES // check the red border of the memory cell if you obtain a negative number!',
          'OUT',
          'HLT',
          'A DAT',
          'B DAT',
          'RES DAT'
        ]
      }, {
        name: 'Multiplication',
        id: 'times',
        code: [
          'INP',
          'BRZ END',
          'STA A',
          'INP',
          'BRZ END',
          'STA B',
          'START BRZ END',
          'LDA RES',
          'ADD A',
          'STA RES',
          'LDA B',
          'SUB ONE',
          'STA B',
          'BRA START',
          'END LDA RES',
          'OUT',
          'HLT',
          'A DAT',
          'B DAT',
          'ONE DAT 1',
          'RES DAT',
        ]
      }, {
        name: 'Division',
        id: 'division',
        code: [
          'INP',
          'STA A',
          'INP',
          'STA B',
          'LDA A',
          'SUB B',
          'BRP START',
          'BRZ START',
          'LDA A // swap if A < B, just to do always A/B :)',
          'STA SWAP',
          'LDA B',
          'STA A',
          'LDA SWAP',
          'STA B // end swap',
          'START LDA A',
          'SUB B',
          'STA A',
          'LDA TIMES',
          'ADD ONE',
          'STA TIMES',
          'LDA A',
          'BRZ END',
          'BRP START',
          'LDA TIMES // here we have a negative TIMES, so adjust it to get the correct result',
          'SUB ONE',
          'STA TIMES',
          'END LDA TIMES',
          'OUT',
          'HLT',
          'A DAT',
          'B DAT',
          'TIMES DAT',
          'ONE DAT 1',
          'RES DAT',
          'SWAP DAT',
        ]
      }, {
        name: 'Even/odd',
        id: 'even-odd',
        code: [
          'INP',
          'STA A',
          'START LDA A',
          'SUB B',
          'STA A',
          'BRZ END',
          'BRP START',
          'LDA ODD',
          'OUT',
          'HLT',
          'END LDA EVEN',
          'OUT',
          'HLT',
          'A DAT',
          'B DAT 2',
          'EVEN DAT 1',
          'ODD DAT 0',
        ]
      }
    ];
  }
}
