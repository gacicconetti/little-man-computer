import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Examples} from '../../common/examples';

enum VarType { variable, label, labelInstruction }

enum Status {running = 'Running', waiting = 'Waiting', stopped = 'Stopped', paused = 'Paused', stepbystep = 'Step by step'}

@Component({
  selector: 'app-simulator',
  templateUrl: './simulator.component.html',
  styleUrls: ['./simulator.component.sass'],
  encapsulation: ViewEncapsulation.None,
})
export class SimulatorComponent implements OnInit {

  selectedRow;
  logLevel = 'normal';
  darkEnabled = false;
  copyTCMessage = 'COPY TO CLIPBOARD';
  examples;
  programCounter;
  instructionRegister;
  addressRegister;
  executionTime;
  speed = 200;
  start;
  sbs = false;
  oldStatus;
  currentCommandLine;
  commandsText = [''];
  caretPosition = {top: 0, left: 0};
  caretVisible = false;
  memory = new Array(10);
  variables: Array<{ name: string, value: number, instruction: string, line: number, type: VarType }> = [];
  userInput;
  log = [];
  accumulator: { value: number, isNegative: boolean };
  outputs = [];
  accumulatorNegative = false;
  availableCommands = [
    {
      code: 'ADD',
      location: '1'
    }, {
      code: 'SUB',
      location: '2'
    }, {
      code: 'STA',
      location: '3'
    }, {
      code: 'LDA',
      location: '5'
    }, {
      code: 'BRA',
      location: '6'
    }, {
      code: 'BRZ',
      location: '7'
    }, {
      code: 'BRP',
      location: '8'
    }, {
      code: 'INP',
      location: '901'
    }, {
      code: 'OUT',
      location: '902'
    }, {
      code: 'HLT',
      location: '000'
    }, {
      code: 'DAT',
      location: ''
    }
  ];
  properties = [
    'boxSizing',
    'width',
    'height',
    'overflowX',
    'overflowY',
    'borderTopWidth',
    'borderRightWidth',
    'borderBottomWidth',
    'borderLeftWidth',
    'paddingTop',
    'paddingRight',
    'paddingBottom',
    'paddingLeft',
    'fontStyle',
    'fontVariant',
    'fontWeight',
    'fontStretch',
    'fontSize',
    'lineHeight',
    'fontFamily',
    'textAlign',
    'textTransform',
    'textIndent',
    'textDecoration',
    'letterSpacing',
    'wordSpacing'
  ];
  mirrorDiv;
  computed;
  style;
  isFirefox;
  isCodeValid;
  caret;
  status: Status = Status.waiting;


  constructor() {
    this.examples = new Examples();
  }

  ngOnInit() {
    this.isFirefox = !(window['mozInnerScreenX'] == null);
    this.createMemory();
    this.caret = (document.getElementsByClassName('caret')[0] as HTMLElement);
    console.log(this.memory);
  }

  selectRow(i, j) {
    var s = (i * 10) + j;
    this.selectedRow = this.selectedRow == s ? null : s;
  }

  selectedExample($event) {
    if ($event.target.value == '') {
      this.commandsText = [];
      this.resetState();
    } else {
      var code = this.examples.examples.find(e => e.id == $event.target.value).code;
      this.commandsText = code;
      (document.getElementById('real-commands') as HTMLTextAreaElement).value = code.join('\n');
      this.validate();
    }
  }

  createMemory() {
    for (var i = 0; i < 10; i++) {
      this.memory[i] = new Array<{ value: string, isCommand: boolean, isNegative: boolean }>(10);
    }
  }

  newLine() {
  }

  commandKey($event) {
    this.updateCaretPosition();
    if ($event.key == 'Enter') {
      this.commandsText.push('');
      this.updateCaretPosition();
    }
    var val = $event.target.value;
    this.commandsText = val.split(/\n/);
    //console.log(this.commandsText);
  }


  updateCaretPosition() {
    var element = document.getElementById('real-commands') as HTMLTextAreaElement;
    var coordinates = this.getCaretCoordinates(element, element.selectionEnd);
    this.caretPosition = {
      top: element.offsetTop
        - element.scrollTop
        + coordinates.top,
      left: element.offsetLeft
        - element.scrollLeft
        + coordinates.left
    };
    if (this.caretPosition.top < 0 || this.caretPosition.top > element.offsetHeight - this.caret.offsetHeight + 2 || !this.caretVisible) {
      this.caret.style.display = 'none';
    } else {
      this.caret.style.display = 'block';
    }
  }

  backLine($e) {
    $e.preventDefault();
    for (var i = 0; i < this.commandsText.length; i++) {
      if (this.commandsText[i] == undefined) {
        this.commandsText.splice(i, 1);
      }
    }
  }

  getCaretCoordinates(element, position) {
    // mirrored div
    this.mirrorDiv = document.getElementById(element.nodeName + '--mirror-div');
    if (!this.mirrorDiv) {
      this.mirrorDiv = document.createElement('div');
      this.mirrorDiv.id = element.nodeName + '--mirror-div';
      document.body.appendChild(this.mirrorDiv);
    }

    this.style = this.mirrorDiv.style;
    this.computed = getComputedStyle(element);

    this.style.whiteSpace = 'pre-wrap';
    this.style.wordWrap = 'break-word';
    // position off-screen
    this.style.position = 'absolute';  // required to return coordinates properly
    this.style.top = element.offsetTop + parseInt(this.computed.borderTopWidth) + 'px';
    this.style.left = 0;
    this.style.top = 0;
    this.style.transform = 'translate(-200%, -200%)';

    // transfer the element's properties to the div
    this.properties.forEach((prop) => {
      this.style[prop] = this.computed[prop];
    });

    if (this.isFirefox) {
      this.style.width = parseInt(this.computed.width) - 2 + 'px';  // Firefox adds 2 pixels to the padding - https://bugzilla.mozilla.org/show_bug.cgi?id=753662
      // Firefox lies about the overflow property for textareas: https://bugzilla.mozilla.org/show_bug.cgi?id=984275
      if (element.scrollHeight > parseInt(this.computed.height)) {
        this.style.overflowY = 'scroll';
      }
    } else {
      this.style.overflow = 'scroll';  // for Chrome to not render a scrollbar; IE keeps overflowY = 'scroll'
    }

    this.mirrorDiv.textContent = element.value.substring(0, position);

    var span = document.createElement('span');
    // Wrapping must be replicated *exactly*, including when a long word gets
    // onto the next line, with whitespace at the end of the line before (#7).
    // The  *only* reliable way to do that is to copy the *entire* rest of the
    // textarea's content into the <span> created at the caret position.
    // for inputs, just '.' would be enough, but why bother?
    span.textContent = element.value.substring(position) || '.';  // || because a completely empty faux span doesn't render at all
    this.mirrorDiv.appendChild(span);

    return {
      top: span.offsetTop + parseInt(this.computed['borderTopWidth']),
      left: span.offsetLeft + parseInt(this.computed['borderLeftWidth'])
    };
  }

  replaceComment(inner, withString = null) {
    if (inner.indexOf('//') == -1) {
      return inner;
    }
    var comment = inner.substr(inner.indexOf('//'), inner.length);
    if (withString == null) {
      return inner.replace(comment, '<span class="comment">' + comment + '</span>');
    }
    return inner.replace(comment, withString);
  }

  replaceCommand(inner) {
    var s = this.replaceComment(inner, '').trim().split(' ');
    var res = inner;
    s.forEach(e => {
      var command = this.availableCommands.find(e1 => e1.code == e);
      if (command != null) {
        res = inner.replace(command.code, '<span class="command">' + command.code + '</span>');
      }
    });
    return res;
  }

  replaceNumber(inner) {
    var s = this.replaceComment(inner, '').trim().split(' ');
    var res = inner;
    s.forEach(e => {
      if (!isNaN(e)) {
        res = inner.replace(e, '<span class="number">' + e + '</span>');
      }
    });
    return res;
  }

  getCommandString(s) {
    var c = this.replaceNumber(
      this.replaceCommand(
        this.replaceComment(s)
      )
    );
    return c != '' ? c : '&nbsp;';
  }

  hasCommand(s) {
    for (var i = 0; i < this.availableCommands.length; i++) {
      if (s.indexOf(this.availableCommands[i].code) > -1) {
        return {hasCommand: true, command: this.availableCommands[i].code};
      }
    }
    return {hasCommand: false, command: null};
  }


  setUpVariables() {
    this.commandsText.forEach((e, i) => {
      var commands = this.getCommands(e);
      if (this.commandIsVariable(commands)) {
        console.log(commands);
        this.variables.push({
          name: commands[0],
          value: commands.length > 2 ? parseInt(commands[2]) : 0,
          line: i,
          instruction: null,
          type: VarType.variable
        });
      }
      if (this.commandIsLabel(commands)) {
        this.variables.push({
          name: commands[0],
          value: commands[1],
          line: i,
          instruction: null,
          type: VarType.label
        });
      }
    });
  }

  setUpLabelInstruction() {
    this.commandsText.forEach((e, i) => {
      var commands = this.getCommands(e);
      if (this.commandIsLabelInstruction(commands)) {
        this.variables.push({
          name: commands[0],
          value: commands[2],
          line: i,
          instruction: commands[1],
          type: VarType.labelInstruction
        });
      }
    });
  }

  commandIsVariable(commands) {
    return (commands.length == 2 && commands[1] == 'DAT') || (commands.length == 3 && commands[1] == 'DAT' && /\d/.test(commands[2]) && parseInt(commands[2]) <= 999);
  }

  commandIsLabel(commands) {
    return commands.length == 2 &&
      this.availableCommands.find(e => e.code == commands[1]) != null &&
      this.availableCommands.find(e => e.code == commands[0]) == null && commands[1] != 'DAT';
  }

  commandIsLabelInstruction(commands) {
    return commands.length == 3 &&
      this.availableCommands.find(e => e.code == commands[1]) != null &&
      this.availableCommands.find(e => e.code == commands[0]) == null &&
      commands[1] != 'DAT'; //&& this.variables.find(e => e.name == commands[2]) != null;
  }

  getCommands(e) {
    var temp = this.replaceComment(e, '').trim().split(' ');
    var res = [];
    temp.forEach(c => {
      if (c != '') {
        res.push(c);
      }
    });
    return res;
  }

  isInstruction(commands) {
    return (this.availableCommands.find(e => e.code == commands[0]) != null && commands.length == 2 &&
      (commands[0] !== 'INP' && commands[0] !== 'OUT' && commands[0] !== 'HLT'));
  }

  isAloneInstruction(commands) {
    return (this.availableCommands.find(e => e.code == commands[0]) != null && commands.length == 1 &&
      (commands[0] == 'INP' || commands[0] == 'OUT' || commands[0] == 'HLT'));
  }

  resetState() {
    this.log = [];
    this.outputs = [];
    this.variables = [];
    this.executionTime = null;
    this.programCounter = null;
    this.instructionRegister = null;
    this.addressRegister = null;
    this.accumulator = null;
    this.currentCommandLine = 0;
    this.start = null;
  }

  clear() {
    if (this.status == Status.running) {
      return;
    }
    this.commandsText = [''];
    (document.getElementById('real-commands') as HTMLTextAreaElement).value = '';
    this.resetState();
    this.status = Status.waiting;
    this.createMemory();
  }

  validate() {
    if (this.status == Status.running) {
      return;
    }
    this.resetState();
    this.createMemory();
    if (this.commandsText.length > 100) {
      this.logMessage('You cannot have more than 100 instructions.', true);
      this.isCodeValid = false;
      return;
    }
    if (this.commandsText.length == 1 && this.commandsText[0] == '') {
      this.logMessage('No instruction to run', true);
      this.isCodeValid = false;
      return;
    }
    for (var i = 0; i < this.commandsText.length; i++) {
      var e = this.commandsText[i];
      if (!this.hasCommand(e) && e.indexOf('//') > -1) {
        this.logMessage('Error at line: ' + i + '. Lines with comment only are not supported, it\'s like you are trying to allocate a comment in memory, which is incorrect :)', true);
        this.isCodeValid = false;
        return;
      }
    }
    this.setUpVariables();
    this.setUpLabelInstruction();
    console.log(this.variables);
    this.isCodeValid = true;
    var i = 0;
    this.commandsText.forEach((e, index) => {
      var commands = this.getCommands(e);
      if (commands.length > 0) {
        var isInstruction = (this.availableCommands.find(e => e.code == commands[0]) != null && commands.length == 2 &&
          (commands[0] !== 'INP' && commands[0] !== 'OUT' && commands[0] !== 'HLT'));
        var isAloneInstruction = (this.availableCommands.find(e => e.code == commands[0]) != null && commands.length == 1 &&
          (commands[0] == 'INP' || commands[0] == 'OUT' || commands[0] == 'HLT'));
        var isVariable = this.commandIsVariable(commands);
        var isLabel = this.commandIsLabel(commands);
        var isLabelInstruction = this.commandIsLabelInstruction(commands);
        //console.log(isAloneInstruction, isInstruction, isVariable, isLabelInstruction, isLabel, this.replaceComment(e, '').trim());
        if (!isAloneInstruction && !isInstruction && !isVariable && !isLabel && !isLabelInstruction) {
          this.logMessage('Error at line ' + (i + 1) + ': "' + this.replaceComment(e, '').trim() + '" is not a known instruction.', true);
          this.isCodeValid = false;
        }
        if (this.isCodeValid) {
          var currentCommand = null;
          for (var commandIndex = 0; commandIndex < commands.length; commandIndex++) {
            currentCommand = this.availableCommands.find(e => e.code == commands[commandIndex]);
            if (currentCommand) {
              break;
            }
          }
          var memoryValue = {};
          if (isAloneInstruction) {
            memoryValue = {value: currentCommand.location, isCommand: true, isNegative: false};
          }
          if (isLabelInstruction) {
            var v = this.variables.find(e => e.name == commands[2]);
            if (!v) {
              this.logMessage('Error at line ' + (i + 1) + ': variable ' + commands[2] + ' not found', true);
              this.isCodeValid = false;
            } else {
              var line = v.line;
              memoryValue = {value: currentCommand.location + (line < 10 ? '0' + line : line), isCommand: true, isNegative: false};
            }
          }
          if (isInstruction) {
            if (!isNaN(commands[1])) {
              memoryValue = {
                value: currentCommand.location + (parseInt(commands[1]) < 10 ? '0' + Math.abs(commands[1]) : Math.abs(commands[1])),
                isCommand: true,
                isNegative: parseInt(commands[1]) < 0
              };
            } else {
              var v = this.variables.find(e => e.name == commands[1]);
              if (!v) {
                this.logMessage('Error at line ' + (i + 1) + ': variable ' + commands[1] + ' not found', true);
                this.isCodeValid = false;
              } else {
                var line = v.line;
                memoryValue = {value: currentCommand.location + (line < 10 ? '0' + line : line), isCommand: true, isNegative: false};
              }
            }
          }
          if (isLabel) {
            var v = this.variables.find(e => e.name == commands[0]);
            if (!v) {
              this.logMessage('Error at line ' + (i + 1) + ': variable ' + commands[0] + ' not found', true);
              this.isCodeValid = false;
            } else {
              var line = v.line;
              memoryValue = {value: currentCommand.location, isCommand: true, isNegative: false};
            }
          }
          if (isVariable) {
            var v = this.variables.find(e => e.name == commands[0]);
            if (!v) {
              this.logMessage('Error at line ' + (i + 1) + ': variable ' + commands[0] + ' not found', true);
              this.isCodeValid = false;
            } else {
              var val = Math.abs(v.value);
              memoryValue = {
                value: val < 10 ? '00' + val : val < 100 ? '0' + val : val + '',
                isCommand: (val + '').length == 3,
                isNegative: v.value < 0
              };
            }
          }
          this.memory[Math.floor(i / 10)][i % 10] = memoryValue;
        }
        i++;
      }
    });
    if (this.isCodeValid) {
      this.logMessage('All OK, ready to run!', false, true);
    }
    this.status = Status.waiting;
    console.log(this.memory);
  }

  run() {
    if (this.status == Status.running) {
      this.status = Status.paused;
      return;
    } else if (this.status == Status.paused || this.status == Status.stepbystep) {
      this.status = Status.running;
      this.sbs = false;
      this.nextCommand(this.currentCommandLine);
      return;
    }
    this.validate();
    if (this.isCodeValid) {
      this.resetState();
      this.start = new Date().getTime();
      this.status = Status.running;
      this.executeCommand(this.memory[0][0], 0);
    }
  }

  stop() {
    this.validate();
    this.status = Status.stopped;
  }

  nextCommand(line) {
    var l = parseInt(line);
    console.log('Going to line:', line);
    if (l + 1 < this.commandsText.length) {
      setTimeout(() => {
        this.executeCommand(this.memory[Math.floor((l + 1) / 10)][(l + 1) % 10], l + 1);
      }, this.sbs ? 0 : this.speed);
    }
  }

  step() {
    if (!this.isCodeValid) {
      this.validate();
    }
    if (this.isCodeValid) {
      if (!this.start) {
        this.currentCommandLine = -1;
      }
      this.start = new Date().getTime();
      this.nextCommand(this.currentCommandLine);
    }
  }

  executeCommand(e, line_) {
    if (this.status == Status.stopped || this.status == Status.paused) {
      return;
    }
    var line = parseInt(line_);
    this.currentCommandLine = line;
    console.log('Executing command', e);
    e.value = e.value + '';
    var ins = e.value.charAt(0);
    this.programCounter = line;
    var cell = parseInt(e.value.charAt(1) + e.value.charAt(2));
    this.instructionRegister = ins;
    this.addressRegister = e.value.charAt(1) + e.value.charAt(2);
    var isInstruction = ins == '1' || ins == '2' || ins == '3' || ins == '5' ||
      ins == '6' || ins == '7' || ins == '8';
    var isAloneInstruction = e.value == '901' || e.value == '902' || e.value == '000';
    var isVariable = ins == '0' && !e.isCommand;
    if (isVariable) {
      this.nextCommand(line);
    }
    if (isAloneInstruction) {
      this.handleAloneInstruction(e, line);
      if (e.value == '000' && e.isCommand) {
        this.executionTime = new Date().getTime() - this.start;
        this.status = Status.stopped;
        this.logMessage('Program terminated', false, true);
        return;
      } else {
        if (!this.sbs) {
          this.nextCommand(line);
        }
      }
    }
    if (isInstruction) {
      this.handleInstruction(ins, cell, line);
    }
  }

  handleInstruction(command, cell, line) {
    var m = this.memory[Math.floor(cell / 10)][cell % 10];
    switch (command) {
      case '3':
        var v = this.addZeroToAccumulator(this.accumulator);
        if (!m) {
          this.memory[Math.floor(cell / 10)][cell % 10] = {
            value: v,
            isCommand: v.length == 3,
            isNegative: this.accumulator.isNegative
          };
        } else {
          this.memory[Math.floor(cell / 10)][cell % 10].value = v;
          this.memory[Math.floor(cell / 10)][cell % 10].isNegative = this.accumulator.isNegative;
        }
        if (this.logLevel == 'debug') {
          this.logMessage('Storing accumulator value: ' + v + ' - cell: ' + cell);
        }
        if (!this.sbs) {
          this.nextCommand(line);
        }
        break;
      case '5':
        if (this.logLevel == 'debug') {
          this.logMessage('Storing cell value ' + m.value + ' in accumulator');
        }
        this.accumulator = this.removeZeroForAccumulator(m);
        if (!this.sbs) {
          this.nextCommand(line);
        }
        break;
      case '1':
        var val = this.removeZeroForAccumulator(m);
        var av = this.accumulator.isNegative ? -this.accumulator.value : this.accumulator.value;
        var vv = val.isNegative ? -val.value : val.value;
        var newVal = av + vv;
        if (Math.abs(newVal) > 999) {
          this.logMessage('Overflow at line: ' + cell + ' for value: ' + newVal, true);
          return;
        }
        if (this.logLevel == 'debug') {
          this.logMessage('Doing ' + av + ' + ' + vv);
        }
        this.accumulator = {value: Math.abs(newVal), isNegative: newVal < 0};
        if (!this.sbs) {
          this.nextCommand(line);
        }
        break;
      case '2':
        var val = this.removeZeroForAccumulator(m);
        var av = this.accumulator.isNegative ? -this.accumulator.value : this.accumulator.value;
        var vv = val.isNegative ? -val.value : val.value;
        var newVal = av - vv;
        if (Math.abs(newVal) > 999) {
          this.logMessage('Overflow at line: ' + cell + ' for value: ' + newVal, true);
          return;
        }
        if (this.logLevel == 'debug') {
          this.logMessage('Doing ' + av + ' - ' + vv);
        }
        this.accumulator = {value: Math.abs(newVal), isNegative: newVal < 0};
        if (!this.sbs) {
          this.nextCommand(line);
        }
        break;
      case '6':
        if (this.logLevel == 'debug') {
          this.logMessage('BRA to ' + cell);
        }
        this.executeCommand(m, cell);
        break;
      case '8':
        if (!this.accumulator.isNegative) {
          if (this.logLevel == 'debug') {
            this.logMessage('BRP to ' + cell);
          }
          this.executeCommand(m, cell);
        } else {
          if (!this.sbs) {
            if (this.logLevel == 'debug') {
              this.logMessage('BRP failed, going to ' + (line + 1));
            }
            this.nextCommand(line);
          }
        }
        break;
      case '7':
        if (this.accumulator.value == 0) {
          if (this.logLevel == 'debug') {
            this.logMessage('BRZ to ' + (line + 1));
          }
          this.executeCommand(m, cell);
        } else {
          if (!this.sbs) {
            if (this.logLevel == 'debug') {
              this.logMessage('BRZ failed, going to ' + (line + 1));
            }
            this.nextCommand(line);
          }
        }
        break;
    }
  }

  addZeroToAccumulator(a) {
    return a.value < 10 ? '00' + a.value : a.value < 100 ? '0' + a.value : a.value;
  }

  removeZeroForAccumulator(a): { value: number, isNegative: boolean } {
    var res = '000';
    a.value = a.value + '';
    if (a.value == '000') {
      return {value: 0, isNegative: false};
    }
    var isNegative = a.isNegative;
    for (var i = 0; i < a.value.length; i++) {
      if (a.value.charAt(i) !== '0') {
        res = a.value.substr(i, a.value.length);
        break;
      }
    }
    return {value: parseInt(res), isNegative: isNegative};
  }

  handleAloneInstruction(command, n_line) {
    switch (command.value) {
      case '901':
        do {
          this.userInput = prompt('Type the input!');
        } while (isNaN(this.userInput));
        var input = parseInt(this.userInput);
        this.accumulatorNegative = input < 0;
        this.accumulator = {value: Math.abs(input), isNegative: input < 0};
        break;
      case '902':
        this.outputs.push(this.accumulator.value);
        setTimeout(() => {
          var o = document.getElementById('out');
          o.scrollTop = o.scrollHeight;
        }, 0);
        break;
      case '000':
        break;
    }
  }

  stepByStepChanged() {
    this.sbs = !this.sbs;
    if (this.sbs) {
      this.oldStatus = this.status;
      this.status = Status.stepbystep;
      this.logMessage('Step by step mode enabled', false, true);
    } else {
      this.status = this.oldStatus;
      this.logMessage('Step by step mode disabled', false, true);
      if (this.status == Status.running) {
        this.nextCommand(this.currentCommandLine);
      }
    }
  }

  isCurrentCell(i, j) {
    return Math.floor(this.currentCommandLine / 10) == i && j == this.currentCommandLine % 10;
  }

  logMessage(message, isError = false, isSuccess = false) {
    this.log.push({
      message: message,
      isError: isError,
      isSuccess: isSuccess
    });
    setTimeout(() => {
      var l = document.getElementById('log');
      l.scrollTop = l.scrollHeight;
    }, 0);
  }

  getStatusColor() {
    return this.status == Status.stopped ? 'stopped' : this.status == Status.running ? 'running' : '';
  }

  isRunning() {
    return this.status == Status.running;
  }

  isPaused() {
    return this.status == Status.paused;
  }


  import($event) {
    var fr = new FileReader();
    fr.onload = () => {
      (document.getElementById('real-commands') as HTMLTextAreaElement).value = fr.result as string;
      this.commandKey({target: {value: fr.result as string}});
    };
    fr.readAsText($event.target.files[0]);
  }

  triggerImport() {
    (document.getElementById('file-import') as HTMLInputElement).click();
  }


  export() {
    var data = (document.getElementById('real-commands') as HTMLTextAreaElement).value;
    var filename = 'lmc-' + (new Date().getTime());
    var type = 'text';
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
    {
      window.navigator.msSaveOrOpenBlob(file, filename);
    } else { // Others
      var a = document.createElement('a'),
        url = URL.createObjectURL(file);
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
    this.logMessage('Exported file with name: ' + filename, false, true);
  }

  copyMessage() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = (document.getElementById('real-commands') as HTMLTextAreaElement).value;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.logMessage('Code copied to clipboard!', false, true);
  }

  print() {
    document.getElementById('print').innerHTML = document.getElementById('commands').innerHTML;
    window.print();
  }

}

